import PackageDescription

let package = Package(
  name: "avahitest",
  dependencies: [
    .Package(url: "https://gitlab.com/boterock/CAvahi.git", majorVersion:1),
  ]
)
